/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.photosorter.model;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;

public class DirectoryTest {
	private SimpleDateFormat FORMAT = new SimpleDateFormat("dd/MM/yyyy");

	@Test
	public void testGetOldestFile() throws ParseException, IOException {
		IDateRetriever theDateRetriever = new FileDateRetriever();
		Directory theDirectory = new Directory(null);
		theDirectory.addFile(createFile("01/02/2013"));
		theDirectory.addFile(createFile("27/03/2014"));

		Assert.assertEquals("01/02/2013",
				FORMAT.format(theDirectory.getOldestFileDate(theDateRetriever)));
		Assert.assertEquals("27/03/2014", FORMAT.format(theDirectory
				.getmMostRecentFileDate(theDateRetriever)));
	}

	private File createFile(String aDate) throws ParseException, IOException {
		File theFile = new File(aDate.replace("/", "_"));
		theFile.deleteOnExit();
		if (!theFile.exists()) {
			Assert.assertTrue(theFile.createNewFile());
		}
		Assert.assertTrue(theFile
				.setLastModified(FORMAT.parse(aDate).getTime()));
		return theFile;
	}

	@Test
	public void testModifyLastModified() throws ParseException, IOException {
		Assert.assertEquals("27/03/2013", FORMAT.format(new Date(createFile(
				"27/03/2013").lastModified())));
	}

	@Test
	public void testReadMetaData() throws ImageProcessingException, IOException {
		Metadata metadata = ImageMetadataReader
				.readMetadata(new File(
						"C:/data/projects/p2p/src/test/resources/chabernac/protocol/asyncfiletransfer/mars_1k_color.jpg"));
		ExifSubIFDDirectory directory = metadata
				.getDirectory(ExifSubIFDDirectory.class);
		Date date = directory
				.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);

		ExifIFD0Directory directoryd0 = metadata
				.getDirectory(ExifIFD0Directory.class);
		date = directoryd0.getDate(ExifIFD0Directory.TAG_DATETIME);
		System.out.println(date);
	}

	@Test
	public void generateUid() {
		System.out.println(UUID.randomUUID().toString());
	}
}
