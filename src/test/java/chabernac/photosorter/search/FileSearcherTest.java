package chabernac.photosorter.search;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.junit.Test;

import chabernac.photosorter.model.Directory;

public class FileSearcherTest {
    @Test
    public void testFileSearch() {
        BlockingQueue<Directory> theQueue = new ArrayBlockingQueue<Directory>(
                10000 );
        FileSearcher theSearcher = new FileSearcher( new File( "c:/temp/" ),
                theQueue, new WildcardFileFilter( "*.jpg" ) );
        theSearcher.run();
        for ( Directory theFile : theQueue ) {
            System.out.println( theFile.getDirectory().getAbsolutePath() );
        }
    }

}
