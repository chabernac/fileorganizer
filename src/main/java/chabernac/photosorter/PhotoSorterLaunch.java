package chabernac.photosorter;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import chabernac.photosorter.model.Directory;
import chabernac.photosorter.organizer.PhotoOrganizer;
import chabernac.photosorter.search.FileSearcher;
import chabernac.photosorter.search.ImagesFileFilter;

public class PhotoSorterLaunch {
    public static void main( String args[] ) {
        ArrayBlockingQueue<Directory> theQueue = new ArrayBlockingQueue<Directory>(
                100 );

        ExecutorService theService = Executors.newFixedThreadPool( 2 );
        theService.execute( new FileSearcher( new File( args[ 0 ] ), theQueue,
                new ImagesFileFilter() ) );
        theService.execute( new PhotoOrganizer( new File( args[ 1 ] ), theQueue ).setRunOnFinished( new ExitRunnable() ) );
    }
}
