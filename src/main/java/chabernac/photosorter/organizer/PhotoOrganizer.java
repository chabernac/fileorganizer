package chabernac.photosorter.organizer;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import chabernac.photosorter.model.Directory;
import chabernac.photosorter.model.IDateRetriever;
import chabernac.photosorter.model.ImageDateRetriever;

public class PhotoOrganizer extends AbstractOrganizer {
    private static Logger          LOGGER         = Logger.getLogger( PhotoOrganizer.class.getName() );
    private final SimpleDateFormat YEAR           = new SimpleDateFormat( "yyyy" );
    private final SimpleDateFormat MONTH_DAY      = new SimpleDateFormat( "MM_dd" );
    private final IDateRetriever   _dateRetriever = new ImageDateRetriever();

    public PhotoOrganizer( File aDestinationFolder, BlockingQueue<Directory> aFileQueue ) {
        super( aDestinationFolder, aFileQueue );
    }

    @Override
    protected void organizeDirectory( Directory aDirectory, File aDestinationFolder ) {
        try {
            File theDestinationDirectory = getTargetDirectory( aDirectory, aDestinationFolder );

            System.out.println( "Copying '" + aDirectory.getFiles().size() + "' files from '" +
                                aDirectory.getDirectory().getAbsolutePath() + "' --> '" +
                                theDestinationDirectory.getAbsolutePath() +
                                "'" );

            for ( File theFile : aDirectory.getFiles() ) {
                FileUtils.copyFile( theFile, new File( theDestinationDirectory,
                        theFile.getName() ) );
            }
        } catch ( IOException e ) {
            LOGGER.log( Level.SEVERE, "Could not organize directory '" + aDirectory + "'", e );
        }

    }

    private File getTargetDirectory( Directory aDirectory, File aDestinationFolder ) throws IOException {

        String theStartDateString = MONTH_DAY.format( aDirectory
                .getOldestFileDate( _dateRetriever ) );
        String theHighDateString = MONTH_DAY.format( aDirectory
                .getmMostRecentFileDate( _dateRetriever ) );

        String theSamePart = getSamePart( theStartDateString, theHighDateString );

        String theYear = YEAR.format( aDirectory.getOldestFileDate( _dateRetriever ) );

        String theFileName = aDirectory.getDirectory().getName();

        if ( theSamePart.length() == 0 ) {
            String theDirectoryName = theYear + " (" + theStartDateString + "-"
                                      + theHighDateString + ")";
            theFileName = theFileName.replaceAll( theDirectoryName, "" );
            return new File( aDestinationFolder, theYear + "/"
                                                 + theDirectoryName + " " + theFileName );
        } else if ( theSamePart.length() < theStartDateString.length() ) {

            String thePart1 = theStartDateString
                    .substring( theSamePart.length() + 1 );
            String thePart2 = theHighDateString
                    .substring( theSamePart.length() + 1 );
            String theDirectoryName = theYear + "_" + theSamePart + " ("
                                      + thePart1 + "-" + thePart2 + ")";
            theFileName = theFileName.replaceAll( theDirectoryName, "" );
            return new File( aDestinationFolder, "/" + theYear + "/"
                                                 + theDirectoryName + " " + theFileName );
        } else {
            String theDirectoryName = theYear + "_" + theSamePart;
            theFileName = theFileName.replaceAll( theDirectoryName, "" );
            return new File( aDestinationFolder, "/" + theYear + "/"
                                                 + theDirectoryName + " " + theFileName );
        }

    }

    private String getSamePart( String aString1, String aString2 ) {
        String[] theParts1 = aString1.split( "_" );
        String[] theParts2 = aString2.split( "_" );

        String theSamePart = "";
        for ( int i = 0; i < theParts1.length; i++ ) {
            if ( !theParts1[ i ].equals( theParts2[ i ] ) ) {
                return theSamePart;
            }
            if ( theSamePart.length() > 0 ) {
                theSamePart += "_";
            }
            theSamePart += theParts1[ i ];
        }
        return aString1;
    }
}
