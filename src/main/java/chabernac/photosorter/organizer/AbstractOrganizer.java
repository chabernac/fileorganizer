/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.photosorter.organizer;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import chabernac.photosorter.model.Directory;

public abstract class AbstractOrganizer implements Runnable {
	private static Logger LOGGER = Logger.getLogger(AbstractOrganizer.class
			.getName());
	private final File _destinationFolder;
	private final BlockingQueue<Directory> _fileQueue;
	private boolean stop = false;
	private Runnable _runOnFinished = null;

	public AbstractOrganizer(File aDestinationFolder,
			BlockingQueue<Directory> aFileQueue) {
		super();
		_destinationFolder = aDestinationFolder;
		_fileQueue = aFileQueue;
	}

	public Runnable getRunOnFinished() {
		return _runOnFinished;
	}

	public AbstractOrganizer setRunOnFinished(Runnable aRunOnFinished) {
		_runOnFinished = aRunOnFinished;
		return this;
	}

	@Override
	public void run() {
		while (!stop) {
			try {
				Directory theDirectory = _fileQueue.poll(1, TimeUnit.SECONDS);

				if (theDirectory == Directory.MARKER) {
					System.out
							.println("marker received, remaining queue size '"
									+ _fileQueue.size() + "'");
					if (_runOnFinished != null)
						_runOnFinished.run();
					return;
				}

				if (theDirectory != null) {
					organizeDirectory(theDirectory, _destinationFolder);
				}
			} catch (InterruptedException e) {
				LOGGER.log(Level.SEVERE, "Could not take file", e);
			}
		}
	}

	public void stop() {
		stop = true;
	}

	protected abstract void organizeDirectory(Directory aTake,
			File aDestinationFolder);

}
