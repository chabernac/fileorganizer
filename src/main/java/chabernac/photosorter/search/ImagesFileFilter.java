/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.photosorter.search;

import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;

public class ImagesFileFilter extends WildcardFileFilter {

    public ImagesFileFilter() {
        super( new String[] { "*.jpg", "*.gif", "*.png", "*.bmp", "*.jpeg", "*.tiff", "*.raw" }, IOCase.INSENSITIVE );
    }

}
