package chabernac.photosorter.search;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import chabernac.photosorter.model.Directory;

public class FileSearcher implements Runnable {
    private static final Logger            LOGGER   = Logger.getLogger( FileSearcher.class
                                                            .getName() );
    private final BlockingQueue<Directory> _fileQueue;
    private final FileFilter               _fileFilter;
    private final List<File>               _folders = new ArrayList<File>();

    public FileSearcher( File _searchDirectory,
                         BlockingQueue<Directory> _fileQueue, FileFilter _fileFilter ) {
        super();
        this._fileQueue = _fileQueue;
        this._fileFilter = _fileFilter;
        this._folders.add( _searchDirectory );
    }

    @Override
    public void run() {
        while ( !_folders.isEmpty() ) {
            File theFolder = _folders.remove( 0 );
            // System.out.println( "Scanning folder '"
            // + theFolder.getAbsolutePath() + "' ..." );
            Directory theDirectory = new Directory( theFolder );
            for ( File theFile : theFolder.listFiles() ) {
                if ( theFile.isDirectory() ) {
                    _folders.add( theFile );
                } else if ( _fileFilter.accept( theFile ) ) {
                    theDirectory.addFile( theFile );
                }
            }
            if ( !theDirectory.isEmpty() ) {
                // System.out.println( "Directory found with files '" + theDirectory.getDirectory().getAbsolutePath() + "'" );
                put( theDirectory );
            }
        }
        System.out.println( "put marker" );
        put( Directory.MARKER );
    }

    private void put( Directory aDirectory ) {
        _fileQueue.offer( aDirectory );
    }
}
