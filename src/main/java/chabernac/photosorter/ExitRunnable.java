/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.photosorter;

public class ExitRunnable implements Runnable {

    @Override
    public void run() {
        System.exit( 0 );
    }

}
