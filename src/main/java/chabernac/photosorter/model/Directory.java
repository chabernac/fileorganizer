package chabernac.photosorter.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Directory {
    public static final Directory MARKER = new Directory( null );

    private final File            _directory;
    private final List<File>      _files = new ArrayList<File>();

    public Directory( File aDirectory ) {
        super();
        this._directory = aDirectory;
    }

    public File getDirectory() {
        return _directory;
    }

    public void addFile( File aFile ) {
        _files.add( aFile );
    }

    public List<File> getFiles() {
        return Collections.unmodifiableList( _files );
    }

    public boolean isEmpty() {
        return _files.isEmpty();
    }

    public File getOldestFile( IDateRetriever aDateRetriever ) throws IOException {
        File theOldestFile = null;
        for ( File theFile : _files ) {
            if ( theOldestFile == null
                 || aDateRetriever.obtainDate( theFile ).getTime() < aDateRetriever.obtainDate( theOldestFile ).getTime() ) {
                theOldestFile = theFile;
            }
        }
        return theOldestFile;
    }

    public File getMostRecentFile( IDateRetriever aDateRetriever ) throws IOException {
        File theMostRecenttFile = null;
        for ( File theFile : _files ) {
            if ( theMostRecenttFile == null
                 || aDateRetriever.obtainDate( theFile ).getTime() > aDateRetriever.obtainDate( theMostRecenttFile ).getTime() ) {
                theMostRecenttFile = theFile;
            }
        }
        return theMostRecenttFile;
    }

    public Date getOldestFileDate( IDateRetriever aDateRetriever ) throws IOException {
        return aDateRetriever.obtainDate( getOldestFile( aDateRetriever ) );
    }

    public Date getmMostRecentFileDate( IDateRetriever aDateRetriever ) throws IOException {
        return aDateRetriever.obtainDate( getMostRecentFile( aDateRetriever ) );
    }

    @Override
    public String toString() {
        return "<directory location='" + _directory.getAbsolutePath() + "'/>";
    }
}
