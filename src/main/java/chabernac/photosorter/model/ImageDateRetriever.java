/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.photosorter.model;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;

public class ImageDateRetriever implements IDateRetriever {
    private static Logger LOGGER = Logger.getLogger( ImageDateRetriever.class.getName() );

    @Override
    public Date obtainDate( File aFile ) {
        try {
            Metadata metadata = ImageMetadataReader.readMetadata( aFile );

            ExifSubIFDDirectory directory = metadata.getDirectory( ExifSubIFDDirectory.class );
            if ( directory != null ) {
                Date theDate = directory.getDate( ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL );
                if ( theDate != null ) return theDate;
            }

            ExifIFD0Directory directoryd0 = metadata.getDirectory( ExifIFD0Directory.class );
            if ( directoryd0 != null ) {
                Date theDate = directoryd0.getDate( ExifIFD0Directory.TAG_DATETIME );
                if ( theDate != null ) return theDate;
            }
        } catch ( ImageProcessingException | IOException e ) {
            // LOGGER.log( Level.INFO, "Unable to obtain image metadata from '" + aFile.getAbsolutePath() + "'" );
        }

        return new Date( aFile.lastModified() );
    }
}
