/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.photosorter.model;

import java.io.File;
import java.util.Date;

public interface IDateRetriever {
    public Date obtainDate( File aFile );
}
